﻿namespace TurnerChallenge1.Models
{
    public class Title
    {
        public string _id { get; set; }

        public int TitleId { get; set; }

        public string TitleName { get; set; }

        public string TitleNameSortable { get; set; }

        public int ReleaseYear { get; set; }

        public object Awards { get; set; }

        public object Genres { get; set; }

        public object OtherNames { get; set; }

        public object Participants { get; set; }

        public object Storylines { get; set; }

        public object Keywords { get; set; }

        public object KeyGenres { get; set; }
    }
}