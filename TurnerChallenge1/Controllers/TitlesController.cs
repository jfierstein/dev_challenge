﻿using DAL;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TurnerChallenge1.Models;

namespace TurnerChallenge1.Controllers
{
    public class TitlesController : ApiController
    {
        MongoDAL mongDal;

        public TitlesController()
        {
            mongDal = new MongoDAL();
        }
         
        public async Task<List<Title>> Get()
        {
            return await mongDal.GetAll();
        }

        public async Task<Title> Get([FromUri(Name = "name")]int titleId)
        {          
            Title title = await mongDal.GetTitle(titleId);
            if (title != null)
                return title;
            else throw new HttpResponseException(HttpStatusCode.NotFound);            
        }

        [HttpGet]
        public async Task<List<Title>> Search([FromUri(Name = "name")]string titleName)
        {
            return await mongDal.SearchTitleByName(titleName);
        }

    }
}