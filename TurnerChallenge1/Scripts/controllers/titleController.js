﻿(function () {
    angular.module('app').controller('titleController', function ($scope, $http, titleService) {
        $scope.titles = new Array();
        $scope.showPoster = true;
        $scope.posterURL = "";
        $scope.search = function () {
            titleService.search($scope)
        };
        $scope.clear = function () {
            $scope.searchTerm = "";
            titleService.search($scope)
        };
    });
})();