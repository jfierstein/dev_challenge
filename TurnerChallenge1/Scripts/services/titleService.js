﻿(function () {
    /* Angular service to communciate with /api/titles */
    angular.module('app').service('titleService', function ($http) {
        this.search = function (scope) {
            scope.titles = new Array();
            scope.isLoading = true;
            return $http({
                method: 'GET',
                url: 'api/titles/search/' + scope.searchTerm,
            }).then
            (function (response) //SUCCESS (titleapi)
            {
                //TO-DO break this out into somewhere more organized
                var getPosterURL = function (title, year, id) {
                    return $http({
                        method: 'GET',
                        url: "http://www.omdbapi.com/?t=" + title + "&y=" + year
                    }).then
                    (function (response) //SUCCESS (omdbapi)
                    {
                        if (response.data.Poster)
                            $("#posterImg_" + id).html("<img class=\"img-thumbnail\" src=\"" + response.data.Poster + "\"></img>")
                        else $("#posterImg_" + id).html("");
                    },
                    function (data) //ERROR (omdbapi)
                    {
                        var statusCodes =
                        {
                            404: function () { } //custom 404 error
                        };
                        statusCodes[data.status]();
                    });
                };
                scope.titles = response.data;
                for(var i=0; i<scope.titles.length; i++){
                    getPosterURL(scope.titles[i].TitleName, scope.titles[i].ReleaseYear, scope.titles[i].TitleId);
                }
            },
            function (data) //ERROR (titleapi)
            {
                var statusCodes =
                {
                    404: function () { } //custom 404 error
                };
                statusCodes[data.status]();
            }).then
            (function () { //COMPLETE (titleapi)
                scope.isLoading = false;
                scope.noRecordsFound = (scope.titles.length == 0);
            });         
        };
    });
})();