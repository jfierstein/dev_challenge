﻿using MongoDB.Driver;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TurnerChallenge1.Models;

namespace DAL
{
    public class MongoDAL
    {
        MongoClient mongoClient;
        IMongoCollection<Title> collection;

        public MongoDAL() 
        {
            var connectionString = "mongodb://readonly:turner@ds043348.mongolab.com:43348/dev-challenge";
            mongoClient = new MongoClient(connectionString);
            var db = mongoClient.GetDatabase("dev-challenge");
            collection = db.GetCollection<Title>("Titles");
        }

        public async Task<List<Title>> GetAll()
        {
           return await collection.Find(_ => true).ToListAsync();
        }

        public async Task<Title> GetTitle(int titleId)
        {
            var results = await collection.Find(Builders<Title>.Filter.Eq("TitleId", titleId)).ToListAsync();
            return results.FirstOrDefault();
        }

        public async Task<List<Title>> SearchTitleByName(string titleName)
        {
            return await collection.Find(Builders<Title>.Filter.Regex("TitleName", new BsonRegularExpression(".*" + titleName + ".*", "i"))).ToListAsync();
        }
    }
}
